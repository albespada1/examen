package Actividad_2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AProbarTest {

	@Test
	void testCuadrado() {
		AProbar a = new AProbar();
		assertEquals(a.cuadrado(4), 16);
		//fail("Not yet implemented");
	}

	@Test
	void testPregunta() {
		AProbar a = new AProbar();
		assertEquals(a.pregunta("Pepe"), "Pepe, ¿Cómo estás?");
		//fail("Not yet implemented");
	}

	@Test
	void testIguales() {
		AProbar a = new AProbar();
		assertEquals(a.iguales(2, 5), false);
		//fail("Not yet implemented");
	}

}
