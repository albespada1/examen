package Actividad_2;

/**
 * <h2>Clase AProbar</h2>
 * 
 * Esta clase contiene un metodo Main junto a otros tres metodos propios los
 * cuales son cuadrado(), pregunta() y iguales()
 * 
 * @author Alberto
 * @version 1.0
 */
public class AProbar {

	/**
	 * Metodo Main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// vacio
	}

	/**
	 * Metodo que multiplica el numero dado por si mismo
	 * 
	 * @param x
	 * @return un numero multiplicado por si mismo
	 */
	public static int cuadrado(int x) {
		return x * x;
	}

	/**
	 * Metodo que retorna una cadena String dada, junto a otra fija: ¿Como estas?
	 * 
	 * @param s
	 * @return Retorna una cadena tipo String
	 */
	public static String pregunta(String s) {
		return s + ", ¿Cómo estás?";
	}

	/**
	 * Metodo que compara dos numero dados, comparan si son iguales
	 * 
	 * @param x
	 * @param y
	 * @return true si los numero son iguales, false si los numeros no son iguales
	 */
	public static boolean iguales(int x, int y) {
		return (x == y);
	}
}